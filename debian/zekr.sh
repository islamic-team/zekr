#!/bin/sh

args=$*

ZEKR_WORKSPACE=~/.zekr

# Parse arguments:
while [ ! -z $1 ]; do
  case "$1" in
    -workspace)
      ZEKR_WORKSPACE=$2
      shift
      ;;
    -index)
      ZEKR_WARNINGS="disable"
      [ ! "$2" = "all" ] || NO_WORKSPACE=1
      shift
      ;;
    -clean)
      ZEKR_WARNINGS="disable"
      [ ! "$2" = "index-all" ] || NO_WORKSPACE=1
      shift
      ;;
  esac
  shift
done

# If workspace dir. does not exist, create it:
[ -d "${ZEKR_WORKSPACE}" ] || [ $NO_WORKSPACE ] || mkdir -p ${ZEKR_WORKSPACE}

ZEKR_CONF_FILE=${ZEKR_WORKSPACE}/zekrrc-debian

# Source system wide configuration file first,
[ ! -f /etc/zekr/zekrrc ] || . /etc/zekr/zekrrc
# then user configuration file
[ ! -f ${ZEKR_CONF_FILE} ] || . ${ZEKR_CONF_FILE}

# Check if suitable Qur'an font is available:
if [ ! "${ZEKR_WARNINGS}" = "disable" ]; then
  MEQRN=1
  if [ -z $(fc-list me_quran) ]; then
    MEQRN=0
    zenity --warning \
      --title="ttf-me-quran is not installed." \
      --text="Uthman-Taha experimental theme cannot be used.\nInstall ttf-me-quran to enable Uthman-Taha theme."
  fi
  if [ "$MEQRN" -eq 0 ]; then
    zenity --question \
      --title="Show Warning messages Next Time" \
      --text="Do you want to leave warning message enabled?"

    if [ $? -eq 1 ]; then
      if grep ZEKR_WARNINGS "${ZEKR_CONF_FILE}" > /dev/null 2>&1 ; then
        sed 's/ *ZEKR_WARNINGS.*=.*/ZEKR_WARNINGS=disable/' -i "${ZEKR_CONF_FILE}"
      else
        echo 'ZEKR_WARNINGS=disable' >> "${ZEKR_CONF_FILE}"
      fi

    fi
  fi
fi


# using eclipse's method for setting ZEKR_JAVA_HOME
# If the user has not set ZEKR_JAVA_HOME, cycle through our list of compatible VM's
# and pick the first one that exists.
if [ -z "${ZEKR_JAVA_HOME}" -a ! -n "${ZEKR_JAVA_CMD}" ]; then
#    echo "searching for compatible vm..."
    javahomelist=`cat /etc/zekr/java_home  | grep -v '^#' | grep -v '^$' | while read line ; do echo -n $line ; echo -n ":" ; done`
    OFS="$IFS"
    IFS=":"
    for ZEKR_JAVA_HOME in $javahomelist ; do
#        echo -n "  testing ${ZEKR_JAVA_HOME}..."
        if [ -x "${ZEKR_JAVA_HOME}/bin/java" ]; then
          break
        fi
    done
    IFS="$OFS"
fi

# If we don't have a ZEKR_JAVA_HOME yet, we're doomed.
if [ -z "${ZEKR_JAVA_HOME}" -a ! -n "${ZEKR_JAVA_CMD}" ]; then
    zenity --error \
        --title="Could not launch Zekr" \
        --text="A suitable Java Virtual Machine for running the Zekr could not be located."
    exit 1
fi

# Set ZEKR_JAVA_CMD from ZEKR_JAVA_HOME
if [ -n "${ZEKR_JAVA_HOME}" -a -z "${ZEKR_JAVA_CMD}" ]; then
    ZEKR_JAVA_CMD="$ZEKR_JAVA_HOME/bin/java"
fi

run () {
MAIN_CLASS=net.sf.zekr.ZekrMain
JRE_OPT=-Djava.library.path=/usr/lib/jni/
CLASS_PATH=/usr/share/java/log4j-1.2.jar:/usr/share/java/swt4.jar:/usr/share/java/commons-collections3.jar:/usr/share/java/commons-codec.jar:/usr/share/java/commons-configuration.jar:/usr/share/java/commons-lang.jar:/usr/share/java/commons-io.jar:/usr/share/java/commons-logging.jar:/usr/share/java/velocity.jar:/usr/share/java/lucene3-highlighter.jar:/usr/share/java/lucene3-core.jar:/usr/share/java/lucene3-analyzers.jar:/usr/share/java/lucene3-memory.jar:/usr/share/java/lucene3-misc.jar:/usr/share/java/jl.jar:/usr/share/java/basicplayer.jar:/usr/share/java/tritonus_share.jar:/usr/share/java/jorbis.jar:/usr/share/java/jogg.jar:/usr/share/java/jspeex.jar:/usr/share/java/mp3spi.jar:/usr/share/java/vorbisspi.jar:dist/zekr.jar:
VM_ARGS="-Xms10m -Xmx80m ${EXTRA_VM_ARGS}"

"$ZEKR_JAVA_CMD" $VM_ARGS -cp "$CLASS_PATH" $JRE_OPT $MAIN_CLASS $*
}

DIR_NAME=`dirname $0`
cd $DIR_NAME
cd ../share/zekr/

run $args
exit 0

